import AttendeesList from "./AttendeesList";
import Nav from "./Nav";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import { BrowserRouter, Route, Routes } from "react-router-dom"
import AttendConferenceForm from "./AttendeeConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";


function App(props) {
  if (!props.attendees) {
    return null;
  }
  return (

    <BrowserRouter>
      <Nav/>
      <div className="container">
      <Routes>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm/>}/>
        </Route>
        <Route path="locations">
          <Route path="new" element={<LocationForm/>}/>
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList attendees={props.attendees}/>}/>
          <Route path="new" element={<AttendConferenceForm/>}/>
        </Route>
        <Route path = "presentations">
          <Route path="new" element={<PresentationForm/>} />
        </Route>
        <Route index element={<MainPage/>} />
        <Route path="*"/>
      </Routes>
      </div>
  </BrowserRouter>

  );
}

export default App;
