import { useEffect, useState } from 'react';

function ConferenceForm () {

    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('')
    const [maxAttendees, setMaxAttendees] = useState('')
    const [location, setLocation] = useState('')


    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value)
    }
    const handleStartChange = (event) => {
      const value = event.target.value;
      setStart(value)
    }
    const handleEndChange = (event) => {
      const value = event.target.value;
      setEnd(value)
    }
    const handleDescriptionChange = (event) => {
      const value = event.target.value;
      setDescription(value)
    }
    const handleMaxPresChange = (event) => {
      const value = event.target.value;
      setMaxPresentations(value)
    }
    const handleMaxAttendeesChange = (event) => {
      const value = event.target.value;
      setMaxAttendees(value)
    }
    const handleLocationChange = (event) => {
      const value = event.target.value;
      console.log(value)
      setLocation(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
       };

       const response = await fetch(conferenceUrl, fetchConfig);
       console.log("just fetched")
       if (response.ok) {
        const newConference = await response.json()


        setName('');
        setStart('');
        setEnd('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');
        setLocation('');

       }
    }

    const fetchData = async () => {
      const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations)


          }
    }
    useEffect(() => {
      fetchData();
    }, []);

    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input placeholder="Name" onChange={handleNameChange} required type="text" id="name" name="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Starts" onChange={handleStartChange} required type="date" id="starts" name="starts" className="form-control" value={start}/>
                <label htmlFor="room_count">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Ends" onChange={handleEndChange} required type="date" id="ends" name="ends" className="form-control"value={end} />
                <label htmlFor="city">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <h6>Decsription</h6>
                <textarea placeholder="Description" onChange={handleDescriptionChange} required id="description" name="description" className="form-control" value={description}></textarea>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Name" required type="number" onChange={handleMaxPresChange} id="max_presentations" name="max_presentations" className="form-control" value={maxPresentations}/>
                <label htmlFor="name">Maximun presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Name" required type="number" id="max_attendees" onChange={handleMaxAttendeesChange} name="max_attendees" className="form-control" value={maxAttendees} />
                <label htmlFor="name">Maximun attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" name="location" className="form-select" value={location}>
                  <option  value="">Choose location</option>
                  {locations.map(location =>{
                    return (
                      <option key={location.id} value={location.id} >
                          {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm
