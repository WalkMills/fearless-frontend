import { useEffect, useState } from "react";



function PresentationForm () {

    const [conferences, setConferences] = useState([])
    const [Pname, setPname] = useState("");
    const [Cname, setCname] = useState("");
    const [Pemail, setPemail] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conference, setConference] = useState("");

    const handleChange = (setFunction) => {
        return (
            function (event) {
                setFunction(event.target.value)

            }
        )
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.presenter_name = Pname;
        data.presenter_email = Pemail;
        data.company_name = Cname;
        data.title = title;
        data.synopsis = synopsis

        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json()
            console.log(newPresentation)
            setPname('')
            setCname('')
            setPemail('')
            setTitle('')
            setSynopsis('')
            setConference('')

            document.getElementById('create-presentation-form').reset()
        }
    }

    const fetchData  = async () => {
        const url = 'http://localhost:8000/api/conferences/'

        const response = await fetch(url);

        if (response.ok){
            const data = await response.json()
            setConferences(data.conferences)
            }
        }

        useEffect(() => {
            fetchData();
          }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleChange(setPname)}  placeholder="presenter name" required type="text" id="presenter_name" name="presenter_name" className="form-control"/>
                <label htmlFor="name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange(setPemail)} placeholder="Starts" required type="email" id="presenter_email" name="presenter_email" className="form-control"/>
                <label htmlFor="room_count">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange(setCname)} placeholder="Ends" required type="test" id="company_name" name="company_name" className="form-control"/>
                <label htmlFor="city">Company name</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={handleChange(setTitle)} placeholder="Name" required type="text" id="title" name="title" className="form-control"/>
                  <label htmlFor="name">Title</label>
                </div>
                <div className="form-floating mb-3">
                  <h6>Synopsis</h6>
                  <textarea onChange={handleChange(setSynopsis)} placeholder="Synopsis" required id="synopsis" name="synopsis" className="form-control"></textarea>
                </div>
              <div className="mb-3">
                <select onChange={handleChange(setConference)} required id="conference" name="conference" className="form-select">
                  <option value="">Choose a Conference</option>
                  {conferences.map(conference => {
                      return (
                      <option key={conference.id} value={conference.id}>
                          {conference.name}
                      </option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default PresentationForm;
