window.addEventListener('DOMContentLoaded', async () =>{
    const url = 'http://localhost:8000/api/locations/'

    try {
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();

            const optionTag = document.getElementById('location');

            for (let locs of data.locations) {
                const option = document.createElement('option')

                option.value = locs.id

                option.innerHTML = locs.name

                optionTag.appendChild(option)
            }

        }else{throw new Error("API status returned false")}
    }
    catch(e) {
        console.error(e)
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault()

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)


        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: json,
            header: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok){
            formTag.reset()
            const newConference = await response.json()
            console.log(newConference);
        }
    });

});
