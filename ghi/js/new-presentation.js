window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'

    try {
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json()

            const optionTag = document.getElementById('conference');

            for (let conf of data.conferences) {
                const option = document.createElement('option');

                option.value = conf.id


                option.innerHTML = conf.name

                optionTag.appendChild(option)
            }
        }else{throw new Error("API status returned false")}
    }
    catch(e){
        console.error(e)
    }



    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        console.log("Form data: " + formData)
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(Object.fromEntries(formData))
        const formJson = Object.fromEntries(formData)

        const presentationUrl = `http://localhost:8000/api/conferences/${formJson.conference}/presentations/`
        console.log(presentationUrl);
        console.log(formJson.conference);
        const fetchConfig = {
            method: 'post',
            body: json,
            header: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if(response.ok){
            formTag.reset()
            const newPresentation = await response.json()
            console.log(newPresentation)
        }
    })


});
